import React from 'react'
import {mount} from 'enzyme'
import User from './User'

const nullAuth = () => ""

describe("User component", () => {
  const aUser = {
    avatar: 'http://example.org/url/to/user/avatar/image',
    fullName: "John Doe",
    email: "john.doe@example.org",
  }

  describe("user information", () => {
    const userComponent = mount(<User user={aUser} authFn={nullAuth}/>)

    it("displays the avatar", () => {
      const images = userComponent.find("img")
      expect(images.length).toBe(1)
      expect(images.props().src).toBe(aUser.avatar)
    })

    it("displays the full name", () => {
      componentContainsText(userComponent, `You're logged in as:${aUser.fullName}`)
    })

    it("displays the email", () => {
      componentContainsText(userComponent, aUser.email)
    })
  })

  describe("login", () => {
    it("calls auth function on change account click", () => {
      const authSpy = newAuthSpy()

      const userComponent = mount(<User authFn={authSpy.auth} user={aUser} />)
      userComponent.find(".btn").simulate("click")

      expect(authSpy.nbOfCalls()).toBe(1)
    })
  })
})

function componentContainsText(component, textToMatch) {
  expect(component.text().match(textToMatch).length).toBe(1)
}

function newAuthSpy() {
  var nbOfCalls = 0
  return {
    auth: () => nbOfCalls++,
    nbOfCalls: () => nbOfCalls
  }
}