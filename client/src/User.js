import React from "react"
import PropTypes from "prop-types"

const User = ({user, authFn}) => {
  return (
    <div className="row valign-wrapper">
      <div className="col s2">
        <img className="circle responsive-img" alt="avatar"
             src={user.avatar}/>
      </div>
      <div className="col s10">
        <p className="flow-text">
          <strong>You're logged in as:</strong><br/>
          {user.fullName} <strong>{user.email}</strong><br/>
          <button className="btn" onClick={authFn}>
            Change account
          </button>
        </p>
      </div>
    </div>
  )
}

User.propTypes = {
  user: PropTypes.shape({
    avatar: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    fullName: PropTypes.string.isRequired,
  }),
  authFn: PropTypes.func.isRequired,
}

export default User
