import React from 'react'
import ReactDOM from 'react-dom'
import Auth0Lock from "auth0-lock"
import './index.css'
import App from './App'

const {auth0Config, user, tfUrl} = window

const lock = new Auth0Lock(auth0Config.clientID, auth0Config.domain, {
  auth: {
    redirectUrl: auth0Config.redirectUri,
  }
})

const authFn = () => lock.show()

ReactDOM.render(<App authFn={authFn} tfUrl={tfUrl} user={user}/>, document.getElementById('root'));
