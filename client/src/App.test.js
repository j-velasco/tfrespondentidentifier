import React from 'react'
import {mount} from 'enzyme'
import App from './App'

const emptyUser = {token: ""}
const existingUser = {
  token: "abcd23234",
  avatar: "https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg",
  email: "jhon.doe@example.org",
  fullName: "Jhon Doe",
}
const aTypeformUrl = "http://alias.typeform.com/to/abcd"

describe('App', () => {
  it('calls auth function if no user is provided', () => {
    const callsToAuthFn = []
    const authFnSpy = newAuthFnSpy(callsToAuthFn)
    mount(<App authFn={authFnSpy} user={emptyUser}/>)

    expect(callsToAuthFn.length).toBe(1)
  })

  it('embed the typeform', () => {
    const app = mount(<App authFn={() => false} tfUrl={aTypeformUrl} user={existingUser}/>)
    let iframe = app.find("iframe")

    expect(iframe.length).toBe(1)
    expect(iframe.props().src).toBe(`${aTypeformUrl}?secret=${existingUser.token}`)
  })
})

function newAuthFnSpy(calls) {
  return (...args) => calls.push(args)
}
