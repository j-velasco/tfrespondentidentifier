import React from 'react'
import PropTypes from 'prop-types'

const EmbedTypeform = ({tfUrl}) => {
  return (
    <div className="row">
      <div className="col s12">
        <iframe src={tfUrl} title="Typeform" className="typeform"/>
      </div>
    </div>
  )
}

EmbedTypeform.propTypes = {
  tfUrl: PropTypes.string.isRequired
}

export default EmbedTypeform
