import React from 'react'
import PropTypes from 'prop-types'
import EmbedTypeform from "./TypeformEmbed";
import User from './User'

const App = ({tfUrl, authFn, user}) => {
  if (user.token === "") {
    authFn()
    return <div></div>
  }

  return (
    <div className="App container">
      <User user={user} authFn={authFn}/>
      <EmbedTypeform tfUrl={`${tfUrl}?secret=${user.token}`}/>
    </div>
  )
}

export default App

App.PropTypes = {
  tfUrl: PropTypes.string.isRequired,
  authFn: PropTypes.func.isRequired,
  user: PropTypes.shape({
    token: PropTypes.string.isRequired,
  }),
}
