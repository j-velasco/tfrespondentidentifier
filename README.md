# Typeform respondent identifier
[![Build Status](https://travis-ci.org/j-velasco/tfrespondentidentifier.svg?branch=master)](https://travis-ci.org/j-velasco/tfrespondentidentifier)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/j-velasco/tfrespondentidentifier/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/j-velasco/tfrespondentidentifier/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/g/j-velasco/tfrespondentidentifier/badges/coverage.png?b=master)](https://scrutinizer-ci.com/g/j-velasco/tfrespondentidentifier/?branch=master)

[Typeform](https://www.typeform.com/) is a platform to build and distribute
beautiful forms(aka typeforms) that users engage way more that other
similar products(Typeform consistently show bigger conversion rates). One
thing that typeforms don't provide is a way to identify respondents, so
this project add this functionality leveraging the power of [hidden fields](https://www.typeform.com/help/hidden-fields/)
and [webhooks](https://developer.typeform.com/webhooks/).

## How it works

When a user lands to the home page, the application checks if the user is
logged in, if not prompt the [Auth0 lock](https://auth0.com/lock) to login
and then is redirected back to the application. The application creates
a unique token for the user(only on the first log in) that is use to pass
as a hidden field to the embed typeform. When the app receives the webhook
with the user submission, it validates the identity of the user and "publish"
the message for consumers.

## Usage(WIP)

1. To bring flexibility to the oauth2 provider, the app use [Auth0](https://auth0.com/)
so you need to [create a regular web client](https://auth0.com/docs/clients/client-settings/regular-web-app)
1. Create a [typeform](https://www.typeform.com/help/my-1st-typeform/)
1. Create a Slack incoming webhook
1. Create a Redis database