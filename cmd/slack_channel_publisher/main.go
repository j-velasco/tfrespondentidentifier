package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"github.com/j-velasco/tfrespondentidentifier/pkg/httphandlers"
	"github.com/j-velasco/tfrespondentidentifier/pkg/slack"
	"github.com/j-velasco/tfrespondentidentifier/pkg/redis"
	"golang.org/x/oauth2"

	"html/template"

	_ "github.com/joho/godotenv/autoload"
	"github.com/j-velasco/tfrespondentidentifier/pkg/auth0"
)

func main() {
	ensureEnvVars()

	conf := auth0ConfFromEnv()
	p, err := slack.NewPublisher(os.Getenv("SLACK_WEBHOOK_URL"), os.Getenv("SLACK_MESSAGE_TEMPLATE"))
	if err != nil {
		log.Fatal(err)
	}

	redisClient, err := redis.NewRedisClientFromUrl(os.Getenv("REDIS_URL"))
	if err != nil {
		log.Fatal(err)
	}

	whStorage := redis.NewRedisWebhooksStorage(redisClient)
	usersStorage := redis.NewRedisUserStorage(redisClient)
	oauth2Service := auth0.NewOauth2Service(conf)

	whHandler := pkg.NewWebhookHandler(usersStorage, p, whStorage)

	session := httphandlers.NewSession()
	hh, err := httphandlers.NewHomeHandler(conf, session, template.URL(os.Getenv("TYPEFORM_URL")))
	if err != nil {
		log.Fatal(err)
	}

	hs := httphandlers.Handlers{
		Home:            hh,
		OauthCallback:   httphandlers.NewOauthCallbackHandler(session, usersStorage, pkg.NewPRNG(), oauth2Service),
		TypeformWebhook: http.HandlerFunc(whHandler),
	}

	s := httphandlers.Server(hs, os.Getenv("TYPEFORM_WH_TOKEN"))

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)
	go func() {
		log.Println("Starting service on", s.Addr)
		// service connections
		if err := s.ListenAndServe(); err != nil {
			log.Printf("Error listening: %s\n", err)
			stopChan <- os.Interrupt // stop service
		}
	}()

	<-stopChan
	log.Println("Shutting down server...")

	// shut down gracefully, but wait no longer than 5 seconds before halting
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	s.Shutdown(ctx)

	log.Println("Server stopped")
}

func ensureEnvVars() {
	requiredEnvVars := []string{
		"AUTH0_AUDIENCE",
		"AUTH0_CALLBACK_URL",
		"AUTH0_CLIENT_ID",
		"AUTH0_CLIENT_SECRET",
		"AUTH0_DOMAIN",
		"SLACK_MESSAGE_TEMPLATE",
		"SLACK_WEBHOOK_URL",
		"TYPEFORM_URL",
		"TYPEFORM_WH_TOKEN",
		"SESSION_SECRET",
	}

	for _, e := range requiredEnvVars {
		if os.Getenv(e) == "" {
			log.Printf("Required env var '%s' not found", e)
			os.Exit(1)
		}
	}
}

func auth0ConfFromEnv() *auth0.Config {
	d := os.Getenv("AUTH0_DOMAIN")

	return &auth0.Config{
		OauthConfig: &oauth2.Config{
			ClientID:     os.Getenv("AUTH0_CLIENT_ID"),
			ClientSecret: os.Getenv("AUTH0_CLIENT_SECRET"),
			RedirectURL:  os.Getenv("AUTH0_CALLBACK_URL"),
			Scopes:       []string{"openid", "profile"},
			Endpoint: oauth2.Endpoint{
				AuthURL:  endpoint(d, "/authorize"),
				TokenURL: endpoint(d, "/oauth/token"),
			},
		},
		Domain:       d,
		UserInfo:     endpoint(d, "/userinfo"),
		UserResource: endpoint(d, "/api/v2/users"),
		Audience:     os.Getenv("AUTH0_AUDIENCE"),
	}
}

func endpoint(domain, path string) string {
	return "https://" + domain + path
}
