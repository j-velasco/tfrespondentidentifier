VERSION	?= dev

test-server:
	go test ./...

test-server-with-coverage:
	go test -coverprofile=cover.out ./...
test-client:
	cd client; CI=true yarn test

build:
	go build -o slack_channel_publisher ./cmd/slack_channel_publisher

image:
	docker build -t tfrespondentidentifier:$(VERSION) .

start-redis: stop-redis
	docker run -p "6379:6379" --name typeformsso_redis -d redis:3.2-alpine

stop-redis:
	-docker rm -f typeformsso_redis
