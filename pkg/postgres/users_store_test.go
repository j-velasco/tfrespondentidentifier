package postgres_test

import (
	"github.com/j-velasco/tfrespondentidentifier/pkg/postgres"
	"github.com/fsouza/go-dockerclient"
	"github.com/jmoiron/sqlx"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var (
	dc   *docker.Client
	dbID string
	db   *sqlx.DB
)


var _ = Describe("UsersStore", func() {
	var usersStorage *postgres.UsersStorage
	BeforeSuite(initDB)

	BeforeEach(startPostgresDB)
	AfterEach(stopPostgresDB)

	BeforeEach(func() {
		usersStorage = postgres.NewUsersStorage(db)
	})

	It("it persists users", func() {
		aUser := &pkg.User{
			Email: "user@mail.org",
			Token: "a unique token",
		}
		usersStorage.Save(aUser)
	})
})

func initDB() {
	d, err := docker.NewClientFromEnv()
	Expect(err).ShouldNot(HaveOccurred())
	dc = d

	_, err = dc.ImageHistory("postgres:10-alpine")
	if err == nil {
		return
	}
	Expect(err).To(Equal(docker.ErrNoSuchImage))
	if err != docker.ErrNoSuchImage {
		panic(err)
	}

	err = dc.PullImage(
		docker.PullImageOptions{Repository: "postgres:10-alpine"},
		docker.AuthConfiguration{})
	Expect(err).ShouldNot(HaveOccurred())
}

func startPostgresDB() {
	ports := make(map[docker.Port]struct{})
	dbPort := docker.Port("5432")
	ports[dbPort] = struct{}{}
	bindings := make(map[docker.Port][]docker.PortBinding)
	bindings[dbPort] = []docker.PortBinding{{HostPort: string(dbPort)}}

	res, err := dc.CreateContainer(docker.CreateContainerOptions{
		Config: &docker.Config{
			Image:        "postgres:10-alpine",
			ExposedPorts: ports,
		},
		HostConfig: &docker.HostConfig{
			PortBindings: bindings,
		},
	})
	Expect(err).ShouldNot(HaveOccurred())

	Expect(dc.StartContainer(res.ID, &docker.HostConfig{})).Should(BeNil())
	dbID = res.ID

	time.Sleep(time.Second * 2)

	d, err := sqlx.Connect(
		"postgres",
		"postgres://postgres:postgres@localhost/postgres?sslmode=disable")
	Expect(err).ShouldNot(HaveOccurred())
	db = d
	_, err = db.Exec(`CREATE TABLE consents(
	id TEXT PRIMARY KEY,
	username TEXT NOT NULL,
	access_token TEXT NOT NULL
)`)
	Expect(err).ShouldNot(HaveOccurred())
}

func stopPostgresDB() {
	Expect(dc.StopContainer(dbID, 0)).Should(BeNil())
	Expect(dc.RemoveContainer(docker.RemoveContainerOptions{ID: dbID})).Should(BeNil())
}