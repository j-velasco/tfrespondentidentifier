package postgres

import (
	"github.com/jmoiron/sqlx"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
)

type UsersStorage struct {
	db *sqlx.DB
}

func NewUsersStorage(db *sqlx.DB) *UsersStorage {
	return &UsersStorage{db: db}
}

func (us *UsersStorage) Save(u *pkg.User) error {
	panic("implement me")
}

func (us *UsersStorage) OwnerOfToken(t pkg.UserToken) (*pkg.User, error) {
	panic("implement me")
}
