package pkg

import (
	"fmt"

	"github.com/pkg/errors"
)

type WebhookProcessor struct {
	publisher    ResponsePublisher
	usersStorage UsersStorage
	whStorage    WebhooksStorage
}

type SubmissionToken string
type WebhooksStorage interface {
	RegisterWebhookWithToken(t SubmissionToken) error
	HasWebhookWithToken(t SubmissionToken) (bool, error)
}

type PublishWithoutTokenError struct{}
type TokenNotFoundError struct {
	t string
}
type TokenAlreadyProcessedError struct {
	t string
}
type PublisherError struct {
	err error
}

func newWebhookProcessor(users UsersStorage, p ResponsePublisher, wh WebhooksStorage) WebhookProcessor {
	return WebhookProcessor{usersStorage: users, publisher: p, whStorage: wh}
}

func (h *WebhookProcessor) Process(s Submission) error {
	u, err := h.extractUser(s.Answers)
	if err != nil {
		return errors.WithStack(err)
	}

	submissionToken := SubmissionToken(s.Answers["submission_token"].Answer)
	hasBeenProcessed, err := h.whStorage.HasWebhookWithToken(submissionToken)
	if err != nil {
		return errors.WithStack(err)
	}

	if hasBeenProcessed {
		return errors.WithStack(TokenAlreadyProcessedError{string(submissionToken)})
	}

	s.Respondent = u
	err = h.publisher.Publish(s)
	if err != nil {
		return errors.WithStack(PublisherError{err: err})
	}

	if err := h.whStorage.RegisterWebhookWithToken(submissionToken); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (h *WebhookProcessor) extractUser(answers Answers) (*User, error) {
	token, ok := answers["secret"]
	if !ok {
		return nil, errors.WithStack(PublishWithoutTokenError{})
	}

	u, err := h.usersStorage.OwnerOfToken(UserToken(token.Answer))
	if err != nil {
		return nil, errors.WithStack(err)
	}
	if u == nil {
		return nil, errors.WithStack(TokenNotFoundError{t: token.Answer})
	}

	return u, nil
}

func (err PublisherError) Error() string {
	return fmt.Sprintf("Error publishing result: %v", err.err)
}

func (err PublishWithoutTokenError) Error() string {
	return fmt.Sprint("Publish result without token")
}

func (err TokenNotFoundError) Error() string {
	return fmt.Sprintf("Publising with a unexisting token: %s", err.t)
}

func (err TokenAlreadyProcessedError) Error() string {
	return fmt.Sprintf("Duplicated submission %s", err.t)
}
