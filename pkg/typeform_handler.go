package pkg

import (
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const submissionTokenKey = "submission_token"

type Submission struct {
	Answers     Answers
	Respondent  *User
	SubmittedAt time.Time
}

type TypeformHandler http.HandlerFunc

type ResponsePublisher interface {
	Publish(result Submission) error
}

func NewSubmission(ans Answers, res *User, sa time.Time) Submission {
	return Submission{Answers: ans, Respondent: res, SubmittedAt: sa}
}

func NewWebhookHandler(us UsersStorage, p ResponsePublisher, ws WebhooksStorage) TypeformHandler {
	wp := newWebhookProcessor(us, p, ws)
	whParser := newWebhookParser()

	return newWebhookHandler(wp, whParser)
}

func newWebhookHandler(p WebhookProcessor, whParser WebhookParser) TypeformHandler {
	return func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			logrus.WithField("error", err).Error()
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		submission, err := whParser(body)
		if err != nil {
			logrus.WithField("error", err).Error()
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		err = p.Process(submission)
		if err == nil {
			logrus.WithField("submission", submission).Info("submission processed successfully")
			return
		}

		logEntry := logrus.WithField("error", errors.WithStack(err))
		const logMessage = "handling typeform's webhook"
		switch err.(type) {
		case PublishWithoutTokenError:
			logEntry.Warn(logMessage)
			w.WriteHeader(http.StatusBadRequest)
			return
		case TokenNotFoundError:
			logEntry.Warn(logMessage)
			w.WriteHeader(http.StatusBadRequest)
			return
		case TokenAlreadyProcessedError:
			logEntry.Info(logMessage)
		default:
			logEntry.Error(logMessage)
			w.WriteHeader(http.StatusInternalServerError)
		}
	}
}
