package pkg

import "github.com/pkg/errors"

type TokenGenerator interface {
	Generate() (UserToken, error)
}

type Oauth2Service interface {
	UserFromOauthCode(code OauthCode) (*User, error)
}

type OAuthCallbackHandler func(OauthCode) (*User, error)

func NewOAuthCallbackHandler(us UsersStorage, oauth2 Oauth2Service, gen TokenGenerator) OAuthCallbackHandler {
	return func(code OauthCode) (*User, error) {
		user, err := oauth2.UserFromOauthCode(OauthCode(code))
		if err != nil {
			return nil, errors.WithStack(err)
		}

		if len(user.Token) == 0 {
			if user.Token, err = gen.Generate(); err != nil {
				return nil, errors.WithStack(err)
			}

			if err = us.Save(user); err != nil {
				return nil, errors.WithStack(err)
			}
		}

		return user, nil
	}
}
