package pkg

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"strconv"
)

type prng struct {
	randC chan string
	stopC chan bool
}

func NewPRNG() TokenGenerator {
	r := &prng{
		randC: make(chan string),
		stopC: make(chan bool),
	}

	go r.loop()
	return r
}

func (r prng) loop() {
	defer close(r.randC)
	defer close(r.stopC)

	var i int64
	seed := make([]byte, 64)
	if _, err := rand.Read(seed); err != nil {
		panic(err)
	}

	mac := hmac.New(sha256.New, seed)
	for {
		i++
		mac.Write(int64toBytes(i))
		b := mac.Sum(nil)
		select {
		case r.randC <- base64.URLEncoding.EncodeToString(b):
		case <-r.stopC:
			return
		}
	}
}

func (r prng) Generate() (UserToken, error) {
	s := <-r.randC
	if s == "" {
		return "", errors.New("Generating from a closed prng")
	}
	return UserToken(s), nil
}

func (r prng) Close() {
	r.stopC <- true
}

func int64toBytes(i int64) []byte {
	s := strconv.FormatInt(i, 10)
	return []byte(s)
}
