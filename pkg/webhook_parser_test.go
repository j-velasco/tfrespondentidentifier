package pkg

import (
	"testing"
	"reflect"
	"fmt"
	"time"
)

func TestWebhookParser(t *testing.T) {
	p := newWebhookParser()

	t.Run("Acceptance test", func(t *testing.T) {
		parsedSubmission, err := p(webhookFixture(t))

		if err != nil {
			t.Error(err)
		}

		expectedSubmission := expectedSubmissionForFixture(nil)

		if !reflect.DeepEqual(expectedSubmission, parsedSubmission) {
			t.Errorf("Expecting submission %+v, got %+v instead", expectedSubmission, parsedSubmission)
		}
	})

	t.Run("Should support all field types", func(t *testing.T) {
		var cases = map[string]struct {
			id     string
			answer string
			raw    string
		}{
			"date": {
				"leKWga5eHWzT",
				"1983-10-19",
				`{
					"type": "date",
					"date": "1983-10-19",
					"field": {
						"id": "leKWga5eHWzT",
						"type": "date"
					}
				}`,
			},
			"dropdown": {
				"ROvRDxVxLZhb",
				"Foo",
				`{
						"type": "choice",
						"choice": {
							"label": "Foo"
						},
						"field": {
							"id": "ROvRDxVxLZhb",
							"type": "dropdown"
						}
					}`,
			},
			"email": {
				"rvQeCIdqTBz1",
				"jorge@example.org",
				`{
					"type": "email",
					"email": "jorge@example.org",
					"field": {
						"id": "rvQeCIdqTBz1",
						"type": "email"
					}
				}`,
			},
			"file_url": {
				"blYPp2cyJ97i",
				"https://admin.typeform.com/path/to/file.png",
				`{
					"type": "file_url",
					"file_url": "https://admin.typeform.com/path/to/file.png",
					"field": {
						"id": "blYPp2cyJ97i",
						"type": "file_upload"
					}

				}`,
			},
			"website": {
				"IZEWoxXAllGl",
				"http://ola.k.ase",
				`{
					"type": "url",
					"url": "http://ola.k.ase",
					"field": {
						"id": "IZEWoxXAllGl",
						"type": "website"
					}
				}`,
			},
			"legal: accepted": {
				"DlK6rkTGJ0i3",
				"yes",
				`{
					"type": "boolean",
					"boolean": true,
					"field": {
						"id": "DlK6rkTGJ0i3",
						"type": "legal"
					}
				}`,
			},
			"legal: denied": {
				"DlK6rkTGJ0i3",
				"no",
				`{
					"type": "boolean",
					"boolean": false,
					"field": {
						"id": "DlK6rkTGJ0i3",
						"type": "legal"
					}
				}`,
			},
			"yes_no": {
				"SaCe0SwbPx3l",
				"no",
				`{
					"type": "boolean",
					"boolean": false,
					"field": {
						"id": "SaCe0SwbPx3l",
						"type": "yes_no"
					}
				}`,
			},
			"short_text": {
				"zERoOWNbTDTS",
				"hola",
				`{
					"type": "text",
					"text": "hola",
					"field": {
						"id": "zERoOWNbTDTS",
						"type": "short_text"
					}
				}`,
			},
			"long_text": {
				"OBxctIycLmyh",
				"You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.",
				`{
					"type": "text",
					"text": "You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.",
					"field": {
						"id": "OBxctIycLmyh",
						"type": "long_text"
					}
				}`,
			},
			"multiple_choice: with multiple selection and other disabled": {
				"ZOtnEBgDXvIZ",
				"Bar",
				`{
					"type": "choice",
					"choice": {
						"label": "Bar"
					},
					"field": {
						"id": "ZOtnEBgDXvIZ",
						"type": "multiple_choice"
					}
				}`,
			},
			"multiple_choice: with multiple selection disabled and other enabled": {
				"ZOtnEBgDXvIZ",
				"Foo",
				`{
					"type": "choice",
					"choice": {
						"other": "Foo"
					},
					"field": {
						"id": "ZOtnEBgDXvIZ",
						"type": "multiple_choice"
					}
				}`,
			},
			"multiple_choice: with multiple selection enabled and other disabled": {
				"ZOtnEBgDXvIZ",
				"Foo, Bar",
				`{
					"type": "choices",
					"choices": {
						"labels": ["Foo", "Bar"]
					},
					"field": {
						"id": "ZOtnEBgDXvIZ",
						"type": "multiple_choice"
					}
				}`,
			},
			"multiple_choice: with multiple selection and other enabled": {
				"ZOtnEBgDXvIZ",
				"Foo, Bar, Baz",
				`{
					"type": "choices",
					"choices": {
						"labels": ["Foo", "Bar"],
						"other": "Baz"
					},
					"field": {
						"id": "ZOtnEBgDXvIZ",
						"type": "multiple_choice"
					}
				}`,
			},
			//just simple case as combinations were covered in multiple_choice
			"picture_choice": {
				"cUd3e86ZoQjn",
				"Foo 1",
				`{
					"type": "choice",
					"choice": {
						"label": "Foo 1"
					},
					"field": {
						"id": "cUd3e86ZoQjn",
						"type": "picture_choice"
					}
				}`,
			},
			"number": {
				"ICAPDTSoMiJG",
				"123",
				`{
					"type": "number",
					"number": 123,
					"field": {
						"id": "ICAPDTSoMiJG",
						"type": "number"
					}
				}`,
			},
			"opinion_scale": {
				"yTmZsM5klrCd",
				"3",
				`{
					"type": "number",
					"number": 3,
					"field": {
						"id": "yTmZsM5klrCd",
						"type": "opinion_scale"
					}
				}`,
			},
			"rating": {
				"Th8HfeeFHOpk",
				"9",
				`{
					"type": "number",
					"number": 9,
					"field": {
						"id": "Th8HfeeFHOpk",
						"type": "rating"
					}
				}`,
			},
			"payment": {
				"tRkYhhgejurV",
				"[Payer: Jorge, last4: 4242, amount: 1, success: yes]",
				`{
					"type": "payment",
					"payment": {
						"amount": "1",
						"last4": "4242",
						"name": "Jorge",
						"success": true
					},
					"field": {
						"id": "tRkYhhgejurV",
						"type": "payment"
					}
				}`,
			},
		}

		for f, c := range cases {
			example := fmt.Sprintf(`{
			"form_response": {
				"answers": [
					%s
				]
			}
		}`, c.raw)
			submission, err := p([]byte(example))

			if err != nil {
				t.Error(err)
			}

			expectedAnswer := Answer{FieldId: c.id, Answer: c.answer}
			if !reflect.DeepEqual(submission.Answers[c.id], expectedAnswer) {
				t.Errorf("%s: Expected answer '%+v', got '%+v'", f, expectedAnswer, submission.Answers[c.id])
			}
		}
	})

	t.Run("Should parse submission date", func(t *testing.T) {
		wh := `{
			"form_response": {
				"submitted_at": "2017-08-14T13:31:12Z"
			}
		}`

		submission, err := p([]byte(wh))
		if err != nil {
			t.Error(err)
		}

		expectedSubmissionDate, _ := time.Parse(time.RFC3339, "2017-08-14T13:31:12Z")
		if !reflect.DeepEqual(submission.SubmittedAt, expectedSubmissionDate) {
			t.Errorf("Expecting SubmittedAt '%v', got '%v'", expectedSubmissionDate, submission.SubmittedAt)
		}
	})

	t.Run("Should parse hidden fields", func(t *testing.T) {
		wh := `{
			"form_response": {
				"hidden": {
					"hidden1": "Val 1",
					"hidden2": "Val 2"
				}
			}
		}`

		submission, err := p([]byte(wh))
		if err != nil {
			t.Error(err)
		}

		expectedHidden1 := Answer{Answer: "Val 1", FieldId: "hidden1"}
		if !reflect.DeepEqual(submission.Answers["hidden1"], expectedHidden1) {
			t.Errorf("Expected first hidden field answer '%+v', got '%+v'", expectedHidden1, submission.Answers["hidden1"])
		}

		expectedHidden2 := Answer{Answer: "Val 2", FieldId: "hidden2"}
		if !reflect.DeepEqual(submission.Answers["hidden2"], expectedHidden2) {
			t.Errorf("Expected second hidden field answer '%+v', got '%+v'", expectedHidden2, submission.Answers["hidden2"])
		}
	})
}
