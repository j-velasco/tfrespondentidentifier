package httphandlers

import (
	"context"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"github.com/codegangsta/negroni"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"encoding/json"
	"bytes"
	"github.com/j-velasco/tfrespondentidentifier/pkg/auth0"
)

type HomeHandler http.HandlerFunc

type ClientAuthConfig struct {
	Auth0ClientId     string
	Auth0ClientSecret string
	Auth0Domain       string
	Auth0Audience     string
	Auth0CallbackURL  template.URL
}

type UserFromCookieToContextMiddleware negroni.HandlerFunc

func NewHomeHandler(conf *auth0.Config, ss sessionStore, tfUrl template.URL) (http.Handler, error) {
	hh, err := newHomeHandler(conf, tfUrl)
	if err != nil {
		return nil, err
	}

	return negroni.New(
		negroni.HandlerFunc(newUserFromSessionToContextMiddleware(ss)),
		negroni.Wrap(http.HandlerFunc(hh)),
	), nil
}

func newUserFromSessionToContextMiddleware(session sessionStore) UserFromCookieToContextMiddleware {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		u := &pkg.User{}
		s, err := session.Get(r)
		if err == nil {
			if sessionUser, ok := s.Values["user"].(*pkg.User); ok {
				u = sessionUser
			}
		}

		c := context.WithValue(r.Context(), "user", u)
		r = r.WithContext(c)
		next(w, r)
	}
}

func newHomeHandler(conf *auth0.Config, tfUrl template.URL) (HomeHandler, error) {
	p, err := pathToFrontendAssetsDir()
	if err != nil {
		return nil, err
	}

	homeTpl, err := ioutil.ReadFile(fmt.Sprintf("%s/index.html", p))
	if err != nil {
		return nil, err
	}

	homeTpl = interpolateVars(map[string][]byte{
		"%AUTH0_DOMAIN%":       []byte(conf.Domain),
		"%AUTH0_CLIENT_ID%":    []byte(conf.OauthConfig.ClientID),
		"%AUTH0_CALLBACK_URL%": []byte(conf.OauthConfig.RedirectURL),
		"%AUTH0_AUDIENCE%":     []byte(conf.Audience),
	}, homeTpl)

	return func(w http.ResponseWriter, r *http.Request) {
		u, _ := r.Context().Value("user").(*pkg.User)
		user, _ := json.Marshal(u)

		w.Write(interpolateVars(map[string][]byte{
			"%TF_URL%": []byte(tfUrl),
			"%USER%":   []byte(user),
		}, homeTpl))
	}, nil
}

func interpolateVars(varsToReplace map[string][]byte, buf []byte) []byte {
	for k, v := range varsToReplace {
		buf = bytes.Replace(buf, []byte(k), v, -1)
	}

	return buf
}
