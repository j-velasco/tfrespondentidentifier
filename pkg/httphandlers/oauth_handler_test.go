package httphandlers

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/sessions"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
)

func TestCallbackHandlerShouldRedirectOnValidUser(t *testing.T) {
	t.Run("successfully authenticated", func(t *testing.T) {
		rr := httptest.NewRecorder()
		r, err := http.NewRequest("POST", "/callback", nil)
		if err != nil {
			t.Error("Error creating request in test", err)
		}
		user := &pkg.User{Token: pkg.UserToken("1423423")}
		ctx := context.WithValue(context.Background(), "user", user)

		r = r.WithContext(ctx)

		sessionStore := newInMemorySessionStore()
		callback := newCallbackHandler(sessionStore)
		h := http.HandlerFunc(callback)

		h.ServeHTTP(rr, r)

		s, _ := sessionStore.Get(r)
		u, ok := s.Values["user"]
		if !ok {
			t.Error("User not set in session")
		}

		storedUser := u.(*pkg.User)
		if storedUser != user {
			t.Errorf("Expected user in session %v, got %v", user, storedUser)
		}

		if rr.Code != http.StatusSeeOther {
			t.Errorf("Expected response code (%d), got (%d) instead", http.StatusSeeOther, rr.Code)
		}

		wantedRedirectLocation := "/"
		loc := rr.Header()["Location"][0]
		if loc != wantedRedirectLocation {
			t.Errorf("Expecting redirection to (%s), got (%s) instead", wantedRedirectLocation, loc)
		}
	})
}

type inMemoryGorrillaSessionStore struct {
	memory map[string]*sessions.Session
}

type inMemorySessionStore struct {
	inMemoryStore *inMemoryGorrillaSessionStore
}

func (ss *inMemorySessionStore) Get(r *http.Request) (*sessions.Session, error) {
	return ss.inMemoryStore.Get(r, "test")
}

func (ss *inMemorySessionStore) Save(r *http.Request, w http.ResponseWriter, s *sessions.Session) error {
	return ss.inMemoryStore.Save(r, w, s)
}

func newInMemorySessionStore() *inMemorySessionStore {
	return &inMemorySessionStore{inMemoryStore: newInMemoryGorillaSessionStore()}
}

func newInMemoryGorillaSessionStore() *inMemoryGorrillaSessionStore {
	return &inMemoryGorrillaSessionStore{memory: make(map[string]*sessions.Session)}
}

func (s *inMemoryGorrillaSessionStore) Get(r *http.Request, name string) (*sessions.Session, error) {
	ss, ok := s.memory[name]
	if !ok {
		return sessions.NewSession(s, name), nil
	}

	return ss, nil
}

func (s *inMemoryGorrillaSessionStore) New(r *http.Request, name string) (*sessions.Session, error) {
	return sessions.NewSession(s, name), nil
}

func (s *inMemoryGorrillaSessionStore) Save(r *http.Request, w http.ResponseWriter, ss *sessions.Session) error {
	s.memory[ss.Name()] = ss
	return nil
}
