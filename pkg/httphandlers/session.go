package httphandlers

import (
	"github.com/gorilla/sessions"
	"os"
	"net/http"
)

type sessionStore interface {
	Get(*http.Request) (*sessions.Session, error)
	Save(r *http.Request, w http.ResponseWriter, s *sessions.Session) error
}

type cookieSessionStore struct {
	secret string
	store  *sessions.CookieStore
}

func NewSession() *cookieSessionStore {
	s := os.Getenv("SESSION_SECRET")
	return &cookieSessionStore{
		secret: s,
		store:  sessions.NewCookieStore([]byte(s)),
	}
}

func (ss *cookieSessionStore) Get(r *http.Request) (*sessions.Session, error) {
	return ss.store.Get(r, ss.secret)
}

func (ss *cookieSessionStore) Save(r *http.Request, w http.ResponseWriter, s *sessions.Session) error {
	return ss.store.Save(r, w, s)
}
