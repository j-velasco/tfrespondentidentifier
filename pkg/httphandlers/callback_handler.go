package httphandlers

import (
	"net/http"

	"github.com/codegangsta/negroni"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"github.com/sirupsen/logrus"
)

func NewOauthCallbackHandler(s sessionStore, us pkg.UsersStorage, gen pkg.TokenGenerator,
	o2s pkg.Oauth2Service) http.Handler {
	oauth := newOAuthMiddleware(pkg.NewOAuthCallbackHandler(us, o2s, gen))
	cb := newCallbackHandler(s)

	return negroni.New(
		negroni.HandlerFunc(oauth),
		negroni.Wrap(cb),
	)
}

func newCallbackHandler(store sessionStore) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, ok := r.Context().Value("user").(*pkg.User)
		if !ok {
			logrus.WithField("user", r.Context().Value("user")).Error("getting user from context")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		s, err := store.Get(r)
		if err != nil {
			logrus.WithField("error", err).Error("getting session")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		s.Values["user"] = user
		err = store.Save(r, w, s)
		if err != nil {
			logrus.WithField("error", err).Error("saving user")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		logrus.WithField("userToken", user.Token).Info("auth callback successfully")
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}
