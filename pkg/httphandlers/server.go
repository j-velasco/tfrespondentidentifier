package httphandlers

import (
	"net/http"
	"os"

	"fmt"

	"encoding/gob"
	"log"
	"path/filepath"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
)

type Handlers struct {
	Home            http.Handler
	OauthCallback   http.Handler
	TypeformWebhook http.Handler
}

func init() {
	gob.Register(&pkg.User{})
}

func Server(hs Handlers, whToken string) *http.Server {
	r := mux.NewRouter()

	r.Handle("/", hs.Home)
	r.Handle("/callback", hs.OauthCallback)
	r.Handle(fmt.Sprintf("/typeform/%s", whToken), hs.TypeformWebhook)

	const publicPath = "/public/"
	handler, err := pathToFrontendAsset(publicPath)
	if err != nil {
		log.Fatalf("Error getting base root for public content: %v", err)
	}
	r.Handle(publicPath+"{p:.*}", handler)

	http.Handle("/", r)

	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	n := negroni.Classic()
	n.UseHandler(r)

	return &http.Server{Addr: ":" + port, Handler: n}
}

func pathToFrontendAsset(publicPath string) (http.Handler, error) {
	staticPath, err := pathToFrontendAssetsDir()
	if err != nil {
		return nil, err
	}

	fs := http.Dir(staticPath)
	fileSvr := http.FileServer(fs)

	return http.StripPrefix(publicPath, fileSvr), nil
}

func pathToFrontendAssetsDir() (string, error) {
	root, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s/%s", root, "client/build"), nil
}
