package httphandlers

import (
	"net/http"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"log"
	"context"
	"github.com/gorilla/sessions"
)

type OAuthMiddleware struct {
	UserStorage pkg.UsersStorage
	Generator   pkg.TokenGenerator
}

type CallbackHandler struct {
	store      sessions.Store
}

type OauthMiddleware func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc)

func newOAuthMiddleware(oauth pkg.OAuthCallbackHandler) OauthMiddleware {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		code := r.URL.Query().Get("code")

		user, err := oauth(pkg.OauthCode(code))

		if err != nil {
			log.Println("[ERROR]", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		ctx := context.WithValue(r.Context(), "user", user)

		next(w, r.WithContext(ctx))
	}
}
