package httphandlers

import (
	"testing"
	"net/http"
	"net/http/httptest"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
)

func TestNewUserFromCookieToContextMiddleware(t *testing.T) {
	t.Run("should call next", func(t *testing.T) {
		session := newInMemorySessionStore()
		middleware := newUserFromSessionToContextMiddleware(session)
		r, _ := http.NewRequest(http.MethodGet, "/", nil)
		rr := httptest.NewRecorder()

		nextSpy := newNextHandlerSpy()

		middleware(rr, r, nextSpy.handlerFn)

		if len(nextSpy.calls) != 1 {
			t.Errorf("Expected calls to nextSpy 1, got %d", len(nextSpy.calls))
		}
	})

	t.Run("should put empty user when no user in session", func(t *testing.T) {
		session := newInMemorySessionStore()
		middleware := newUserFromSessionToContextMiddleware(session)
		r, _ := http.NewRequest(http.MethodGet, "/", nil)
		rr := httptest.NewRecorder()

		nextSpy := newNextHandlerSpy()

		middleware(rr, r, nextSpy.handlerFn)

		_, ok := nextSpy.calls[0].r.Context().Value("user").(*pkg.User)
		if !ok {
			t.Error("User not set in context")
		}
	})

	t.Run("should put user in context with values from session", func(t *testing.T) {
		session := newInMemorySessionStore()
		middleware := newUserFromSessionToContextMiddleware(session)
		r, _ := http.NewRequest(http.MethodGet, "/", nil)
		rr := httptest.NewRecorder()

		u := &pkg.User{}
		s, _ := session.Get(r)
		s.Values["user"] = u
		session.Save(r, rr, s)

		nextSpy := newNextHandlerSpy()

		middleware(rr, r, nextSpy.handlerFn)

		userInContext, _ := nextSpy.calls[0].r.Context().Value("user").(*pkg.User)
		if userInContext != u {
			t.Error("Expected user to be the same from the session", userInContext)
		}
	})
}

type nextCall struct {
	w http.ResponseWriter
	r *http.Request
}

type nextSpy struct {
	calls []nextCall
}

func (spy *nextSpy) handlerFn(w http.ResponseWriter, r *http.Request) {
	spy.calls = append(spy.calls, nextCall{w: w, r: r})
}

func newNextHandlerSpy() nextSpy {
	return nextSpy{calls: make([]nextCall, 0)}
}
