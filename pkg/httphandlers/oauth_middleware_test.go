package httphandlers

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"github.com/codegangsta/negroni"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
)

func TestHandlerOauthMiddlewareShouldGetUserFromAuth0AndAddItToContext(t *testing.T) {
	rr := httptest.NewRecorder()
	req, err := http.NewRequest("GET", fmt.Sprintf("/callback?code=%s", "abcde"), nil)
	if err != nil {
		t.Fatal(err)
	}

	var hasNextBeenCalled bool
	var aUser pkg.User
	middleware := newOAuthMiddleware(newSuccessCallbackHandler(&aUser))
	h := harnessOauthMiddleWare(middleware, func(w http.ResponseWriter, r *http.Request) {
		hasNextBeenCalled = true
		u := r.Context().Value("user")
		if u == nil {
			t.Error("Expected user to be pass on context")
		}
		_, ok := u.(*pkg.User)
		if !ok {
			t.Error("Expected user to be instance of User")
		}
	})

	h.ServeHTTP(rr, req)

	if rr.Result().StatusCode != http.StatusOK {
		t.Errorf("Expect request to be OK, got '%s'", rr.Result().Status)
	}
	if !hasNextBeenCalled {
		t.Error("Expect OauthMiddleware to call next")
	}
}

func newSuccessCallbackHandler(u *pkg.User) pkg.OAuthCallbackHandler {
	return func(code pkg.OauthCode) (*pkg.User, error) {
		return u, nil
	}
}

func harnessOauthMiddleWare(callback OauthMiddleware, fn http.HandlerFunc) http.Handler {
	h := http.Handler(
		negroni.New(
			negroni.HandlerFunc(callback),
			negroni.HandlerFunc(func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
				fn(w, r)
			}),
		),
	)
	return h
}
