package slack

import (
	"bytes"
	"text/template"

	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"time"
)

type composer interface {
	compose(r pkg.Submission) string
}

type textComposer struct {
	tpl string
}

type tplContext struct {
	s pkg.Submission
}

func (c *tplContext) answerTo(ref string) string {
	a, ok := c.s.Answers[ref]
	if !ok {
		return "<no value>"
	}

	return a.Answer
}

func (c *tplContext) submittedAt() string {
	return c.s.SubmittedAt.Format(time.RFC822)
}

// use to verify valid template on construction
var nilFuncMap = template.FuncMap{
	"answerTo":    func(ref string) string { return "" },
	"submittedAt": func() string { return "" },
}

func newComposer(tpl string) (textComposer, error) {
	_, err := template.New("validator").Funcs(nilFuncMap).Parse(tpl)
	if err != nil {
		return textComposer{}, err
	}

	return textComposer{tpl: tpl}, nil
}

func (c textComposer) compose(s pkg.Submission) string {
	var buf bytes.Buffer
	ctx := &tplContext{s: s}

	funcs := template.FuncMap{"answerTo": ctx.answerTo, "submittedAt": ctx.submittedAt}
	tpl, _ := template.New("message").Funcs(funcs).Parse(c.tpl)
	tpl.Execute(&buf, s)

	return buf.String()
}
