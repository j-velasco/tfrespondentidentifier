package slack

import (
	"testing"

	"reflect"

	"github.com/go-errors/errors"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
)

const stubbedMessage = "composed message"

func TestSlackPublisher(t *testing.T) {
	t.Run("should publish composed message", func(t *testing.T) {
		w := newWriterSpy()
		c := newComposerStub()
		p := &publisher{composer: c, writer: w}

		s := pkg.Submission{}

		err := p.Publish(s)
		if err != nil {
			t.Error(err)
		}

		want := []string{stubbedMessage}
		if !reflect.DeepEqual(w.receivedMessages, want) {
			t.Errorf("Expected published messages (%+v), got (%+v)", want, w.receivedMessages)
		}
	})

	t.Run("should propagate errors", func(t *testing.T) {
		w := newFailWriter()
		c := newComposerStub()
		p := &publisher{composer: c, writer: w}

		s := pkg.Submission{}

		err := p.Publish(s)
		if err != w.err {
			t.Errorf("Publisher should propagate errors received from writer, expected error '%s', got '%s'", w.err, err)
		}
	})
}

type spyWriter struct {
	receivedMessages []string
}

func newWriterSpy() *spyWriter {
	return &spyWriter{}
}

func (w *spyWriter) postMessage(message string) error {
	w.receivedMessages = append(w.receivedMessages, message)
	return nil
}

type failWriter struct {
	err error
}

func newFailWriter() *failWriter {
	return &failWriter{errors.New("error writing")}
}

func (w *failWriter) postMessage(message string) error {
	return w.err
}

type composerStub struct {
	returnMsg string
}

func newComposerStub() composerStub {
	return composerStub{stubbedMessage}
}

func (c composerStub) compose(s pkg.Submission) string {
	return c.returnMsg
}
