package slack

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewPublisherValidateIsAnAbsoluteUrl(t *testing.T) {
	_, err := newSlackWriter("")
	want := errors.New("Slack incoming URL must be absolute: ''")
	if err == nil || err.Error() != want.Error() {
		t.Errorf("Expecting error '%s', got '%s'", want, err)
	}
}

func TestSlackWriter(t *testing.T) {
	const (
		argumentMessage = "A message to post"
		whPath          = "/incoming/wh"
	)
	var haveSlackBeenCalled bool

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var slackWh struct {
			Text string `json:"text"`
		}
		defer r.Body.Close()
		message, _ := ioutil.ReadAll(r.Body)
		json.Unmarshal(message, &slackWh)

		ct := r.Header.Get("Content-Type")
		if ct != "application/json" {
			t.Errorf("Expected content type application/json, got (%s) instead", ct)
		}
		if slackWh.Text != argumentMessage {
			t.Errorf("Expected message (%s), got (%s) instead", argumentMessage, message)
		}
		if r.Method != "POST" {
			t.Errorf("Expected method POST, got %s instead", r.Method)
		}
		if r.URL.Path != whPath {
			t.Errorf("Expected method %s, got %s instead", whPath, r.URL.Path)
		}

		haveSlackBeenCalled = true
	}))

	sw, err := newSlackWriter(ts.URL + whPath)
	if err != nil {
		t.Error(err)
	}

	sw.postMessage(argumentMessage)
	if !haveSlackBeenCalled {
		t.Errorf("No call to incoming webhook in Slack was done")
	}
}
