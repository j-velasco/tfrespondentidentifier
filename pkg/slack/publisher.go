package slack

import "github.com/j-velasco/tfrespondentidentifier/pkg"

type publisher struct {
	writer   writer
	composer composer
}

func NewPublisher(slackIncomingUrl string, tpl string) (*publisher, error) {
	c, err := newComposer(tpl)
	if err != nil {
		return nil, err
	}

	w, err := newSlackWriter(slackIncomingUrl)
	if err != nil {
		return nil, err
	}

	return &publisher{writer: w, composer: c}, nil
}

func (p *publisher) Publish(s pkg.Submission) error {
	m := p.composer.compose(s)
	return p.writer.postMessage(m)
}
