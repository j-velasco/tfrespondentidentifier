package slack

import (
	"testing"

	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"time"
)

func TestComposerShouldComposeMessageAccordingTemplate(t *testing.T) {
	ans := pkg.NewAnswers()
	ans.Add("2", "Resultones")
	ans.Add("3", "Mailchimp integration is now available")

	respondent := &pkg.User{Name: pkg.Name("Jorge Velasco"), Email: "jorge@typeform.com"}
	submittedAt, _ := time.Parse(time.RubyDate, "Tue Jan 10 16:10:15 -0500 2006")
	r := pkg.NewSubmission(ans, respondent, submittedAt)

	tpl := `submitted at: {{ submittedAt }}, from: {{ .Respondent.Name }}<{{ .Respondent.Email }}> - {{ answerTo "2" }}: {{ answerTo "3" }}`
	expectedPostedMessage := "submitted at: 10 Jan 06 16:10 -0500, from: Jorge Velasco<jorge@typeform.com> - Resultones: Mailchimp integration is now available"

	c, err := newComposer(tpl)
	if err != nil {
		t.Error(err)
	}
	message := c.compose(r)

	if message != expectedPostedMessage {
		t.Errorf("Expected message (%s), got (%s) instead", expectedPostedMessage, message)
	}
}

func TestComposerShouldNotFailOnMissingValues(t *testing.T) {
	r := pkg.NewSubmission(nil, nil, time.Now())
	tpl := `{{ answerTo "1" }}`

	c, err := newComposer(tpl)
	if err != nil {
		t.Error(err)
	}
	message := c.compose(r)
	expectedMessage := "<no value>"

	if message != expectedMessage {
		t.Errorf("Expecting message '%s', got '%s'", expectedMessage, message)
	}
}
