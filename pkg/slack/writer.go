package slack

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type slackWebhook struct {
	Text string `json:"text"`
}

type writer interface {
	postMessage(message string) error
}

type webhookIncomingWriter struct {
	incomingWhUrl *url.URL
}

func newSlackWriter(slackIncomingUrl string) (*webhookIncomingWriter, error) {
	u, err := url.Parse(slackIncomingUrl)
	if err != nil {
		return nil, err
	}
	if !u.IsAbs() {
		return nil, fmt.Errorf("Slack incoming URL must be absolute: '%s'", slackIncomingUrl)
	}

	return &webhookIncomingWriter{incomingWhUrl: u}, nil
}

func (w *webhookIncomingWriter) postMessage(message string) error {
	b, _ := json.Marshal(slackWebhook{Text: message})
	req, err := http.NewRequest("POST", w.incomingWhUrl.String(), bytes.NewBuffer(b))
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		body, _ := ioutil.ReadAll(res.Body)
		defer res.Body.Close()
		return fmt.Errorf("Bad request to Slack (%d: %s)", res.StatusCode, body)
	}

	return nil
}
