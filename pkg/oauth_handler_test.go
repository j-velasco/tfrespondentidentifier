package pkg

import "testing"

var existingUserWithoutToken = &User{
	Email:  Email("email@example.net"),
	Name:   Name("John Doe"),
	Avatar: Avatar("http://url.to/avatar"),
}

var existingUserWithToken = &User{
	Token:  UserToken("abcde"),
	Email:  Email("email@example.net"),
	Name:   Name("John Doe"),
	Avatar: Avatar("http://url.to/avatar"),
}

func TestOAuthCallbackHandler(t *testing.T) {
	t.Run("should add token to user without one", func(t *testing.T) {
		us := &noopUserStorage{}
		tokenFromGenerator := UserToken("abcd1234")
		gen := newTokenGeneratorStub(tokenFromGenerator)
		oauth2 := newOauth2StubService(existingUserWithoutToken)

		handler := NewOAuthCallbackHandler(us, oauth2, gen)

		u, err := handler(OauthCode("a_code"))
		if err != nil {
			t.Errorf("No error expected, got %s", err)
		}
		if u.Token != tokenFromGenerator {
			t.Errorf("Expeting token '%s', got '%s'", tokenFromGenerator, u.Token)
		}
	})

	t.Run("should just return user if already has a token", func(t *testing.T) {
		us := newFailUserStorage()
		oauth2 := newOauth2StubService(existingUserWithToken)
		gen := newTokenGeneratorStub(UserToken(""))
		handler := NewOAuthCallbackHandler(us, oauth2, gen)

		originalToken := existingUserWithToken.Token
		u, err := handler(OauthCode("a_code"))

		if err != nil {
			t.Errorf("No error expected, got %s", err)
		}
		if u != existingUserWithToken {
			t.Errorf("Expected user '%+v', got '%+v'", existingUserWithToken, u)
		}
		if u.Token != originalToken {
			t.Errorf("Expected user token to be perseve(%s), got new %s", originalToken, u.Token)
		}
	})
}

type oauth2StubService struct {
	u *User
}

func (oauth2 *oauth2StubService) UserFromOauthCode(code OauthCode) (*User, error) {
	return oauth2.u, nil
}

func newOauth2StubService(u *User) *oauth2StubService {
	return &oauth2StubService{u}
}

type tokenGeneratorStub struct {
	t UserToken
}

func (gen *tokenGeneratorStub) Generate() (UserToken, error) {
	return gen.t, nil
}

func newTokenGeneratorStub(t UserToken) *tokenGeneratorStub {
	return &tokenGeneratorStub{t}
}
