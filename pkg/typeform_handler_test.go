package pkg

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
	"time"
)

const FIXTURE_SECRET = "kkjlakdsa"

var nilUserStorage = &noopUserStorage{}
var nilWhStorage = NoopWebhookStorage{}

func TestTypeformHandlerShouldPublishAnswersContainedInAWebhook(t *testing.T) {
	req, err := http.NewRequest("POST", "/typeform", bytes.NewBuffer(webhookFixture(t)))
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	rr := httptest.NewRecorder()
	publisher := newSpyPublisher()
	users := newInMemoryUsersStorage()
	u := &User{Token: FIXTURE_SECRET}
	users.Save(u)

	h := http.HandlerFunc(newWebhookHandler(newWebhookProcessor(users, publisher, nilWhStorage), newWebhookParser()))

	h(rr, req)

	if rr.Result().StatusCode != http.StatusOK {
		t.Errorf("Expected (%d) status, got (%d) instead", http.StatusOK, rr.Result().StatusCode)
	}
	if len(publisher.Calls) != 1 {
		t.Errorf("Expected (%d) publish calls, got (%d)", 1, len(publisher.Calls))
	}

	expectedAnswers := expectedSubmissionForFixture(u)
	if !reflect.DeepEqual(expectedAnswers, publisher.Calls[0]) {
		t.Errorf("Expected published answers (%+v), got (%+v) instead", expectedAnswers, publisher.Calls[0].Answers)
	}
	if publisher.Calls[0].Respondent != u {
		t.Errorf("Expected respondent (%+v), got (%+v) instead", u, publisher.Calls[0].Respondent)
	}
}

func TestTypeformHandlerShouldFailOnPublishError(t *testing.T) {
	req, err := http.NewRequest("POST", "/typeform", bytes.NewBuffer(webhookFixture(t)))
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	rr := httptest.NewRecorder()
	publisher := newFailPublisher()
	h := newWebhookHandler(newWebhookProcessor(nilUserStorage, publisher, nilWhStorage), newWebhookParser())

	h(rr, req)

	if rr.Result().StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected (%d), got (%d) instead", http.StatusInternalServerError, rr.Result().StatusCode)
	}
}

func webhookFixture(t *testing.T) []byte {
	wh, err := ioutil.ReadFile("fixtures/tf_webhook.json")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	return wh
}

func expectedSubmissionForFixture(u *User) Submission {
	expectedAnswers := NewAnswers()
	expectedAnswers.Add("29993170", "kjlsads")
	expectedAnswers.Add("29993424", "Conclude")
	expectedAnswers.Add("50111097", "Barcelona, Paris")
	expectedAnswers.Add("secret", "kkjlakdsa")
	expectedAnswers.Add("submission_token", "1910024195bfdb051cffc7067a35ea92")

	sa, _ := time.Parse(time.RFC3339, "2017-05-04T20:13:19Z")
	return NewSubmission(expectedAnswers, u, sa)
}

type spyPublisher struct {
	Calls []Submission
}

func newSpyPublisher() *spyPublisher {
	return &spyPublisher{}
}

func (p *spyPublisher) Publish(s Submission) error {
	p.Calls = append(p.Calls, s)
	return nil
}

func (p *spyPublisher) reset() {
	p.Calls = make([]Submission, 0)
}

type failPublisher struct{}

func newFailPublisher() *failPublisher {
	return &failPublisher{}
}

func (p *failPublisher) Publish(s Submission) error {
	return fmt.Errorf("publish fail")
}

type noopWebhookProcessor struct{}

func (wp *noopWebhookProcessor) process(answers Answers) error {
	return nil
}

type noopResultsStorage struct{}

func (n noopResultsStorage) WebhookOfToken(t UserToken) (Submission, error) {
	return Submission{}, nil
}

type NoopWebhookStorage struct{}

func (wh NoopWebhookStorage) HasWebhookWithToken(t SubmissionToken) (bool, error) {
	return false, nil
}

func (wh NoopWebhookStorage) RegisterWebhookWithToken(t SubmissionToken) error {
	return nil
}
