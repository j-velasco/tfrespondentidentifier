package auth0

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

type oauth2Service struct {
	config *Config
}

type Config struct {
	OauthConfig  *oauth2.Config
	Domain       string
	UserInfo     string
	UserResource string
	Audience     string
}

type auth0Profile struct {
	UserId        string            `json:"user_id"`
	Name          string            `json:"name"`
	Email         pkg.Email `json:"email"`
	EmailVerified bool              `json:"email_verified"`
	Picture       string            `json:"picture"`
	client        http.Client
}

func NewOauth2Service(c *Config) *oauth2Service {
	return &oauth2Service{config: c}
}

func (s *oauth2Service) UserFromOauthCode(code pkg.OauthCode) (*pkg.User, error) {
	token, err := s.config.OauthConfig.Exchange(context.Background(), string(code))
	if err != nil {
		return nil, err
	}

	// Getting now the userInfo
	client := s.config.OauthConfig.Client(context.Background(), token)
	resp, err := client.Get(s.config.UserInfo)
	if err != nil {
		return nil, err
	}

	profile, err := s.getAuth0Profile(resp)
	if err != nil {
		return nil, err
	}

	if len(profile.Email) == 0 {
		return nil, fmt.Errorf("profile doesn't contains email")
	}

	if !profile.EmailVerified {
		return nil, fmt.Errorf("profile doesn't contains a verified email")
	}

	return profile.domainUser(), nil
}

func (p *auth0Profile) domainUser() *pkg.User {
	return &pkg.User{
		Name:        pkg.Name(p.Name),
		Email:       p.Email,
		SurrogateId: p.UserId,
		Avatar:      pkg.Avatar(p.Picture),
	}
}

func (s oauth2Service) getAuth0Profile(resp *http.Response) (auth0Profile, error) {
	var profile auth0Profile

	raw, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return profile, err
	}

	if err = json.Unmarshal(raw, &profile); err != nil {
		return profile, err
	}

	return profile, nil
}

func (s oauth2Service) getAuth0Profiles(resp *http.Response) ([]auth0Profile, error) {
	var profiles []auth0Profile

	raw, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return profiles, err
	}

	if err = json.Unmarshal(raw, &profiles); err != nil {
		return profiles, err
	}

	return profiles, nil
}

func (s *oauth2Service) client() *http.Client {
	cfg := &clientcredentials.Config{
		TokenURL:       s.config.OauthConfig.Endpoint.TokenURL,
		ClientID:       s.config.OauthConfig.ClientID,
		ClientSecret:   s.config.OauthConfig.ClientSecret,
		EndpointParams: url.Values{"audience": {s.config.Audience}},
	}
	client := cfg.Client(context.Background())
	return client
}
