package auth0

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"regexp"

	"github.com/RangelReale/osin"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"golang.org/x/oauth2"
)

const existingUserWithoutTokenCode = "USER_WITHOUT_TOKEN"
const existingUserWithTokenCode = "USER_WITH_TOKEN"

var existingUserWithoutToken = &auth0User{
	Email:         "email@example.net",
	EmailVerified: true,
	Name:          "Jorge",
	UserId:        "google-oauth2|3435",
}

var existingUserWithToken = &auth0User{
	Email:         "other_email@example.net",
	EmailVerified: true,
	Name:          "Claudia",
	UserId:        "google-oauth2|abcd",
	Metadata:      metadata{Token: "absdnad879012332"},
}

func TestHandlerOauthMiddlewareShouldGetUserFromAuth0AndAddItToContext(t *testing.T) {
	var patchedUser auth0User
	ts := fakeAuth0Server(&patchedUser, t)
	defer ts.Close()

	us := newOauth2Service(ts.URL)
	u, err := us.UserFromOauthCode(existingUserWithoutTokenCode)
	if err != nil {
		t.Error("Error testing UserFromOauthCode", err)
	}

	if u.Token != "" && u.Token != patchedUser.Metadata.Token {
		t.Error("Expected user to be patched with a token")
	}
}

func newOauth2Service(authUrl string) *oauth2Service {
	conf := &Config{
		OauthConfig: &oauth2.Config{
			ClientID:     "AUTH0_CLIENT_ID",
			ClientSecret: "AUTH0_CLIENT_SECRET",
			RedirectURL:  "AUTH0_CALLBACK_URL",
			Scopes:       []string{"openid", "auth0Profile"},
			Endpoint: oauth2.Endpoint{
				AuthURL:  authUrl + "/authorize",
				TokenURL: authUrl + "/oauth/token",
			},
		},
		Domain:       authUrl,
		UserInfo:     authUrl + "/userinfo",
		UserResource: authUrl + "/api/v2/users",
	}

	return NewOauth2Service(conf)
}

func fakeAuth0Server(patchedUser *auth0User, t *testing.T) *httptest.Server {
	oauthServer := newOauthServer()
	const (
		searchEndpoint = "/api/v2/users"
	)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost && r.URL.Path == "/oauth/token" {
			resp := oauthServer.NewResponse()
			defer resp.Close()

			if ar := oauthServer.HandleAccessRequest(resp, r); ar != nil {
				ar.Authorized = true
				oauthServer.FinishAccessRequest(resp, r, ar)
			}
			osin.OutputJSON(resp, w, r)
		} else if r.Method == http.MethodGet && r.URL.Path == "/userinfo" {
			resp := oauthServer.NewResponse()
			defer resp.Close()

			if ir := oauthServer.HandleInfoRequest(resp, r); ir != nil {
				// mimic Auth0 response data
				u, _ := ir.AccessData.UserData.(*auth0User)
				um, _ := json.Marshal(u)
				json.Unmarshal(um, &resp.Output)
				oauthServer.FinishInfoRequest(resp, r, ir)
			}
			osin.OutputJSON(resp, w, r)
		} else if r.Method == http.MethodGet && r.URL.Path == searchEndpoint {
			resp := oauthServer.NewResponse()
			defer resp.Close()

			ir := oauthServer.HandleInfoRequest(resp, r)
			if ir == nil {
				osin.OutputJSON(resp, w, r)
				return
			}

			oauthServer.FinishInfoRequest(resp, r, ir)

			q := r.URL.Query().Get("q")
			if q != "" {
				r := regexp.MustCompile("^app_metadata.token:(.+)$")
				matches := r.FindStringSubmatch(q)
				u, err := oauthStorage.findByAppToken(pkg.UserToken(matches[1]))
				if err != nil {
					t.Error(err)
				}

				if u != nil {
					w.WriteHeader(http.StatusOK)
					encoder := json.NewEncoder(w)
					encoder.Encode(u)
				}
			}
		} else if r.Method == "PATCH" && r.URL.Path == "/api/v2/users/google-oauth2|3435" {
			defer r.Body.Close()
			body, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Println(err)
				return
			}
			json.Unmarshal(body, &patchedUser)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
	}))
	return ts
}

var oauthStorage *TestStorage

func newOauthServer() *osin.Server {
	oauthStorage = NewTestStorage()
	client := &osin.DefaultClient{
		Id:          "AUTH0_CLIENT_ID",
		Secret:      "AUTH0_CLIENT_SECRET",
		RedirectUri: "AUTH0_CALLBACK_URL",
	}
	oauthStorage.SetClient("AUTH0_CLIENT_ID", client)
	oauthStorage.SaveAuthorize(&osin.AuthorizeData{
		Code:        existingUserWithoutTokenCode,
		Client:      client,
		CreatedAt:   time.Now(),
		ExpiresIn:   10000,
		RedirectUri: "AUTH0_CALLBACK_URL",
		UserData:    existingUserWithoutToken,
	})
	oauthStorage.SaveAuthorize(&osin.AuthorizeData{
		Code:        existingUserWithTokenCode,
		Client:      client,
		CreatedAt:   time.Now(),
		ExpiresIn:   10000,
		RedirectUri: "AUTH0_CALLBACK_URL",
		UserData:    existingUserWithToken,
	})

	config := osin.NewServerConfig()
	config.AllowedAuthorizeTypes = osin.AllowedAuthorizeType{osin.CODE, osin.TOKEN}
	config.AllowedAccessTypes = osin.AllowedAccessType{osin.AUTHORIZATION_CODE, osin.REFRESH_TOKEN,
		osin.PASSWORD, osin.CLIENT_CREDENTIALS, osin.ASSERTION}
	config.AllowGetAccessRequest = true
	config.AllowClientSecretInParams = true

	oauthServer := osin.NewServer(config, oauthStorage)
	return oauthServer
}
