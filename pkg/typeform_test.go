package pkg

import (
	"testing"
	"time"
)

func TestWebhookProcessor_extractUser(t *testing.T) {
	t.Run("extractUser", func(t *testing.T) {
		usersStorage := newInMemoryUsersStorage()

		aUser := &User{
			Token: "an existing token",
		}
		usersStorage.Save(aUser)
		whProcessor := newWebhookProcessor(usersStorage, newSpyPublisher(), nilWhStorage)

		t.Run("Should return an error if no user secret token is provided", func(t *testing.T) {
			ans := NewAnswers()

			_, err := whProcessor.extractUser(ans)
			if err == nil {
				t.Error("Expected error on publishing without a user token")
			}
			expectedError := "Publish result without token"
			if err.Error() != expectedError {
				t.Errorf("Expected error (%s), got (%s)", expectedError, err)
			}
		})

		t.Run("Should return an error if the provided user's token is unknown", func(t *testing.T) {
			ans := NewAnswers()
			ans.Add("secret", "an unexisting user id")

			_, err := whProcessor.extractUser(ans)
			if err == nil {
				t.Error("Expected error on publishing without an invalid user id")
			}

			ans.Add("secret", string(aUser.Token))
			_, err = whProcessor.extractUser(ans)
			if err != nil {
				t.Error(err)
			}
		})
	})

	t.Run("Process", func(t *testing.T) {
		const submissionTokenKey = "submission_token"

		aUser := User{}
		usersStub := newStubUserStorage(&aUser)

		publisher := newSpyPublisher()
		whStorage := newInMemoryWebhooksStorage()
		whProcessor := newWebhookProcessor(usersStub, publisher, whStorage)

		t.Run("Should publish results for new webhooks", func(t *testing.T) {
			ans := NewAnswers()
			ans.Add("secret", "a valid user token")
			ans.Add(submissionTokenKey, "another submission token")
			s := NewSubmission(ans, nil, time.Now())
			whProcessor.Process(s)

			if len(publisher.Calls) != 1 {
				t.Errorf("Expected (%d) publish calls, got (%d)", 1, len(publisher.Calls))
			}
		})

		t.Run("Shouldn't publish results for already processed webhooks", func(t *testing.T) {
			const alreadyProcessedSubmissionToken = SubmissionToken("abcd123")
			whStorage.RegisterWebhookWithToken(alreadyProcessedSubmissionToken)
			publisher.reset()

			ans := NewAnswers()
			ans.Add("secret", "a valid user token")
			ans.Add(submissionTokenKey, string(alreadyProcessedSubmissionToken))

			s := NewSubmission(ans, nil, time.Now())
			err := whProcessor.Process(s)
			expectedErr := TokenAlreadyProcessedError{string(alreadyProcessedSubmissionToken)}

			if err.Error() != expectedErr.Error() {
				t.Errorf("Expected error (%v), got (%v)", expectedErr, err)
			}
			if len(publisher.Calls) != 0 {
				t.Errorf("Expected no calls publish calls, got (%d)", len(publisher.Calls))
			}
		})

		t.Run("Should register processed webhooks", func(t *testing.T) {
			publisher.reset()

			ans := NewAnswers()
			ans.Add("secret", "a valid user token")
			submissionId := "a new submission"
			ans.Add(submissionTokenKey, submissionId)

			s := NewSubmission(ans, nil, time.Now())
			err := whProcessor.Process(s)

			if err != nil {
				t.Errorf("Unexepected error: %v", err)
			}
			if len(publisher.Calls) != 1 {
				t.Errorf("Expected 1 call to publish, got (%d)", len(publisher.Calls))
			}
			wasRegisted, err := whStorage.HasWebhookWithToken(SubmissionToken(submissionId))
			if err != nil {
				t.Fatal(err)
			}
			if !wasRegisted {
				t.Errorf("Webhook should be registered after publish it")
			}
		})
	})
}

type InMemoryWebhookStorage struct {
	whs map[SubmissionToken]bool
}

func newInMemoryWebhooksStorage() InMemoryWebhookStorage {
	return InMemoryWebhookStorage{whs: make(map[SubmissionToken]bool)}
}

func (whStorage InMemoryWebhookStorage) HasWebhookWithToken(t SubmissionToken) (bool, error) {
	return whStorage.whs[t], nil
}

func (whStorage InMemoryWebhookStorage) RegisterWebhookWithToken(t SubmissionToken) error {
	whStorage.whs[t] = true
	return nil
}
