package pkg

import (
	"fmt"
	"testing"
)

func TestInMemoryStorage(t *testing.T) {
	s := newInMemoryUsersStorage()
	aUser := &User{
		Token:       "12334",
		Email:       "user@example.org",
		SurrogateId: "provider|id",
	}

	s.Save(aUser)

	userById, _ := s.OwnerOfToken("12334")
	if userById != aUser {
		t.Errorf("Expected user %+v, got %+v", aUser, userById)
	}
}

type noopUserStorage struct{}

type inMemoryUsersStorage struct {
	users []*User
}

type stubUsersStorage struct {
	userToReturn *User
}

type failUserStorage struct{}

func (failUserStorage) Save(u *User) error {
	return fmt.Errorf("fail to save user %+v", u)
}

func (failUserStorage) OwnerOfToken(t UserToken) (*User, error) {
	return nil, fmt.Errorf("fail to get user owner of token %s", t)
}

func newFailUserStorage() failUserStorage {
	return failUserStorage{}
}

func (ur *noopUserStorage) Save(u *User) error {
	return nil
}

func (ur *noopUserStorage) OwnerOfToken(t UserToken) (*User, error) {
	return &User{}, nil
}

func newInMemoryUsersStorage() *inMemoryUsersStorage {
	return &inMemoryUsersStorage{make([]*User, 0)}
}

func (m *inMemoryUsersStorage) Save(u *User) error {
	m.users = append(m.users, u)
	return nil
}

func (m *inMemoryUsersStorage) OwnerOfToken(t UserToken) (*User, error) {
	for _, u := range m.users {
		if u.Token == t {
			return u, nil
		}
	}

	return nil, nil
}

func newStubUserStorage(u *User) stubUsersStorage {
	return stubUsersStorage{userToReturn: u}
}

func (sus stubUsersStorage) Save(u *User) error {
	return nil
}

func (sus stubUsersStorage) OwnerOfToken(t UserToken) (*User, error) {
	return sus.userToReturn, nil
}
