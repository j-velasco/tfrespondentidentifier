package pkg

type Avatar string

type User struct {
	Token       UserToken `json:"token"`
	Email       Email     `json:"email"`
	Name        Name      `json:"name"`
	Avatar      Avatar    `json:"avatar"`
	SurrogateId string
}

type Email string
type Name string
type OauthCode string
type UserToken string

type UsersStorage interface {
	Save(u *User) error
	OwnerOfToken(t UserToken) (*User, error)
}
