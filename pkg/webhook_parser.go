package pkg

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type WebhookParser func(body []byte) (Submission, error)

type Answers map[string]Answer

type Answer struct {
	FieldId string
	Answer  string
}

type whField struct {
	Id string `json:"id"`
}

type whChoice struct {
	Label string `json:"label",omitempty`
	Other string `json:"other",omitempty`
}

type whChoices struct {
	Labels []string `json:"labels"`
	Other  string   `json:"other"`
}

type whPayment struct {
	Name    string `json:"name"`
	Last4   string `json:"last4"`
	Amount  string `json:"amount"`
	Success bool   `json:"success"`
}

type whAnswer struct {
	Type  string  `json:"type"`
	Field whField `json:"field"`

	// each answer will have only one of the following depending on the type
	Boolean bool      `json:"boolean"`
	Choices whChoices `json:"choices,omitempty"`
	Choice  whChoice  `json:"choice,omitempty"`
	Date    string    `json:"date",omitempty`
	Email   string    `json:"email",omitempty`
	FileURL string    `json:"file_url",omitempty`
	Number  int       `json:"number",omitempty`
	Payment whPayment `json:"payment",omitempty`
	Text    string    `json:"text,omitempty"`
	URL     string    `json:"url",omitempty`
}

type formResponse struct {
	Hidden      map[string]string `json:"hidden"`
	Answers     []whAnswer        `json:"answers"`
	SubmittedAt time.Time         `json:"submitted_at"`
	Token       string            `json:"token"`
}

type tfWebhook struct {
	FormResponse formResponse `json:"form_response"`
}

func newWebhookParser() WebhookParser {
	return func(body []byte) (Submission, error) {
		var wh tfWebhook
		if err := json.Unmarshal(body, &wh); err != nil {
			return Submission{}, err
		}

		answers := NewAnswers()
		for _, a := range wh.FormResponse.Answers {
			var ans string

			switch a.Type {
			case "boolean":
				if a.Boolean {
					ans = "yes"
				} else {
					ans = "no"
				}
			case "choice":
				if a.Choice.Other != "" {
					ans = a.Choice.Other
				} else {
					ans = a.Choice.Label
				}
			case "choices":
				choices := a.Choices.Labels
				if a.Choices.Other != "" {
					choices = append(choices, a.Choices.Other)
				}
				ans = strings.Join(choices, ", ")
			case "date":
				ans = a.Date
			case "email":
				ans = a.Email
			case "file_url":
				ans = a.FileURL
			case "number":
				ans = strconv.Itoa(a.Number)
			case "payment":
				success := "yes"
				if !a.Payment.Success {
					success = "no"
				}
				amount, _ := strconv.Atoi(a.Payment.Amount) // TODO handle error
				ans = fmt.Sprintf(
					"[Payer: %s, last4: %s, amount: %d, success: %s]",
					a.Payment.Name,
					a.Payment.Last4,
					amount,
					success,
				)
			case "text":
				ans = a.Text
			case "url":
				ans = a.URL
			}

			answers.Add(a.Field.Id, ans)
		}

		// TODO move hidden fields outside answers
		for key, h := range wh.FormResponse.Hidden {
			answers.Add(key, h)
		}
		// TODO move token outside answers
		answers.Add(submissionTokenKey, wh.FormResponse.Token)

		return NewSubmission(answers, nil, wh.FormResponse.SubmittedAt), nil
	}
}

func NewAnswers() Answers {
	return Answers{}
}

func (ans Answers) Add(fieldId string, answer string) {
	ans[fieldId] = Answer{fieldId, answer}
}
