package redis

import (
	"github.com/fsouza/go-dockerclient"
	"fmt"
	"os"
	"time"
	"net"
	"testing"
)

var d *docker.Client

func startRedisTestContainer() string {
	var err error
	if d == nil {
		d, err = docker.NewClientFromEnv()
		if err != nil {
			fmt.Println("Error creating client", err)
			os.Exit(1)
		}
	}

	const redisPort = "6379"
	// Expose redis port
	ports := make(map[docker.Port]struct{})
	ports[redisPort] = struct{}{}
	bindings := make(map[docker.Port][]docker.PortBinding)
	bindings[redisPort] = []docker.PortBinding{{HostPort: redisPort}}

	err = d.PullImage(docker.PullImageOptions{Repository: "redis:3.2-alpine"}, docker.AuthConfiguration{})
	if err != nil {
		panic(fmt.Errorf("Error pulling redis image: %v", err))
	}

	res, err := d.CreateContainer(docker.CreateContainerOptions{
		Config: &docker.Config{
			Image:        "redis:3.2-alpine",
			ExposedPorts: ports,
		},
		HostConfig: &docker.HostConfig{
			PortBindings: bindings,
		},
	})

	if err != nil {
		panic(fmt.Errorf("Error creating redis container: %v", err))
	}

	if err = d.StartContainer(res.ID, &docker.HostConfig{}); err != nil {
		panic(fmt.Errorf("Error starting redis container: %v", err))
	}

	if err = waitStarted(d, res.ID, 5*time.Second); err != nil {
		panic(fmt.Errorf("Error waiting for redis to start: %v", err))
	}

	if err = waitReachable(fmt.Sprintf(`localhost:%s`, redisPort), 20*time.Second); err != nil {
		panic(fmt.Errorf("Redis container not reacheble: %v", err))
	}

	return res.ID
}

// waitStarted waits for a container to start for the maxWait time.
func waitStarted(client *docker.Client, id string, maxWait time.Duration) error {
	done := time.Now().Add(maxWait)
	for time.Now().Before(done) {
		c, err := client.InspectContainer(id)
		if err != nil {
			break
		}
		if c.State.Running {
			return nil
		}
		time.Sleep(100 * time.Millisecond)
	}
	return fmt.Errorf("cannot start container %s for %v", id, maxWait)
}

func removeContainer(containerId string, t *testing.T)  {
	err := d.StopContainer(containerId, 0)
	if err != nil {
		t.Errorf("Error stopping redis container: %v", err)
	}
	if err := d.RemoveContainer(docker.RemoveContainerOptions{ID: containerId}); err != nil {
		println(err)
	}
}

// waitReachable waits for hostport to became reachable for the maxWait time.
func waitReachable(hostport string, maxWait time.Duration) error {
	done := time.Now().Add(maxWait)
	for time.Now().Before(done) {
		c, err := net.Dial("tcp", hostport)
		if err == nil {
			c.Close()
			return nil
		}
		time.Sleep(100 * time.Millisecond)
	}

	return fmt.Errorf("cannot connect %v for %v", hostport, maxWait)
}
