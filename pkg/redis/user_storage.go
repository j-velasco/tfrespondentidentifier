package redis

import (
	"encoding/json"

	"github.com/go-redis/redis"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"github.com/pkg/errors"
)

type redisUserStorage struct {
	client *redis.Client
}

func NewRedisUserStorage(c *redis.Client) *redisUserStorage {
	return &redisUserStorage{c}
}

func (us *redisUserStorage) Save(u *pkg.User) error {
	j, err := json.Marshal(u)
	if err != nil {
		return errors.WithStack(err)
	}

	if err := us.client.Set(keyForUserToken(u.Token), j, 0).Err(); err != nil {
		return errors.WithStack(err)
	}

	return nil
}

func (us *redisUserStorage) OwnerOfToken(t pkg.UserToken) (*pkg.User, error) {
	res, err := us.client.Get(keyForUserToken(t)).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, errors.WithStack(err)
	}

	var u pkg.User
	if err = json.Unmarshal([]byte(res), &u); err != nil {
		return nil, errors.WithStack(err)
	}

	return &u, nil
}
func keyForUserToken(t pkg.UserToken) string {
	return "user:" + string(t)
}
