package redis

import (
	"testing"

	"github.com/j-velasco/tfrespondentidentifier/pkg"
	"reflect"
)

func TestRedisUserStorage(t *testing.T) {
	t.Run("redis user redis acceptance test", func(t *testing.T) {
		containerId := startRedisTestContainer()
		defer removeContainer(containerId, t)

		c, err := NewRedisClientFromUrl("redis://localhost:6379")
		if err != nil {
			t.Error("Fail to create redis client in tests", err)
		}

		us := NewRedisUserStorage(c)

		u := &pkg.User{
			Token:       pkg.UserToken("abcd123"),
			Email:       pkg.Email("email@example.org"),
			Name:        pkg.Name("Jorge Velasco"),
			Avatar:      pkg.Avatar("http://avatar.url/path"),
			SurrogateId: "abce",
		}

		err = us.Save(u)
		if err != nil {
			t.Error("Fail to save user in tests", err)
		}

		userFromStorage, err := us.OwnerOfToken(u.Token)
		if err != nil {
			t.Error("Fail to get user in tests", err)
		}
		if !reflect.DeepEqual(u, userFromStorage) {
			t.Errorf("Expected user '%+v', got '%+v", u, userFromStorage)
		}
	})
}
