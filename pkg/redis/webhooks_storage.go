package redis

import (
	"time"

	"github.com/go-redis/redis"
	"github.com/j-velasco/tfrespondentidentifier/pkg"
)

func NewRedisWebhooksStorage(c *redis.Client) *RedisWebhooksStorage {
	return &RedisWebhooksStorage{client: c}
}

type RedisWebhooksStorage struct {
	client *redis.Client
}

func (wh RedisWebhooksStorage) RegisterWebhookWithToken(t pkg.SubmissionToken) error {
	if _, err := wh.client.Set(string(t), true, time.Hour*24*3).Result(); err != nil {
		return err
	}

	return nil
}

func (wh RedisWebhooksStorage) HasWebhookWithToken(t pkg.SubmissionToken) (bool, error) {
	cmd := wh.client.Get(string(t))
	_, err := cmd.Result()
	if err != nil {
		if err == redis.Nil {
			return false, nil
		}

		return false, err
	}

	return true, nil
}
