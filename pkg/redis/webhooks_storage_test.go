package redis

import (
	"testing"
	domain "github.com/j-velasco/tfrespondentidentifier/pkg"
)

func TestRedisWebhooksStorage(t *testing.T) {
	if testing.Short() {
		t.Skip("Skipping redis test in short mode")
		return
	}

	containerId := startRedisTestContainer()
	c, err := NewRedisClientFromUrl("redis://localhost:6379")
	if err != nil {
		t.Error("Error creating redis client on tests", err)
	}

	whs:= NewRedisWebhooksStorage(c)

	newToken := domain.SubmissionToken("new token")
	existingToken := domain.SubmissionToken("existing token")

	if err := whs.RegisterWebhookWithToken(existingToken); err != nil {
		t.Errorf("Error registering existing token: %v", err)
	}

	exists, err := whs.HasWebhookWithToken(existingToken)
	if err != nil {
		t.Errorf("Error checking existance of existing submission token: %v", err)
	}
	if !exists {
		t.Errorf("Existing submission should be in the redis")
	}

	exists, err = whs.HasWebhookWithToken(newToken)
	if err != nil {
		t.Errorf("Error checking existance of new submission token: %v", err)
	}
	if exists {
		t.Errorf("New token shouldn't be in the redis")
	}

	removeContainer(containerId, t)
}
