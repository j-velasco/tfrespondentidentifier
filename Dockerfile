FROM golang:1.9-alpine as server_builder

RUN apk update
RUN apk add make

COPY . /go/src/github.com/j-velasco/tfrespondentidentifier
WORKDIR /go/src/github.com/j-velasco/tfrespondentidentifier

RUN make build

FROM node:8.6-alpine as client_builder

RUN apk update
RUN apk add yarn
RUN yarn global add react-scripts

COPY ./client/yarn.lock /client/yarn.lock
COPY ./client/package.json /client/package.json
WORKDIR /client
RUN yarn install -s --prod

COPY ./client /client
RUN yarn build


FROM alpine:latest

RUN apk add --update ca-certificates

COPY --from=server_builder /go/src/github.com/j-velasco/tfrespondentidentifier/slack_channel_publisher /slack_channel_publisher
COPY --from=client_builder /client/build /client/build

CMD ["./slack_channel_publisher"]
